                    <div class="col-md-12">
                        <table class="table table-responsive table-hover table-bordered">
                            <tr>
                                <th>No:</th>
                                <th>Author</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Image</th>
                                <th>Tags</th>
                                <th>Status</th>
                                <th>Comment</th>
                                <th>Date</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                            <?php 
                                $no = 1;
                                $query = "SELECT * FROM `posts`";
                                $result = mysqli_query($connect,$query);
                                while ($row=mysqli_fetch_assoc($result)) {
                                    $post_id = $row['post_id'];
                                    $post_author = $row['post_author'];
                                    $post_title = $row['post_title'];
                                    $post_category_id = $row['post_category_id'];
                                    $post_image = $row['post_image'];
                                    $post_tag = $row['post_tag'];
                                    $post_status = $row['post_status'];
                                    $post_commnet_count= $row['post_commnet_count'];
                                    $post_date = $row['post_date'];
                                
                             ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $post_author ?></td>
                                <td><?php echo $post_title ?></td>
                                <?php 
                                    $query = "SELECT * FROM `categories` WHERE cat_id=$post_category_id";
                                    $cat_result=mysqli_query($connect,$query);
                                    while($row=mysqli_fetch_assoc($cat_result)){
                                        $cat_title=$row['cat_title'];
                                        echo "<td>$cat_title</td>";
                                    }
                                 ?>

                                <td><img src="../image/<?php echo $post_image ?>" withd="120px" height="100px" alt=""></td>
                                <td><?php echo $post_tag ?></td>
                                <td><?php echo $post_status ?></td>
                                <td><?php echo $post_commnet_count ?></td>
                                <td><?php echo $post_date ?></td>
                                <td><a href="post.php?source=edit_post&post_id=<?php echo $post_id ?>" class="btn btn-warning">Update</a></td>
                                <td><a href="post.php?delete_id=<?php echo $post_id ?>" class="btn btn-danger">Delete</a></td>
                            </tr>
                            <?php 
                                }
                             ?>
                        </table>
                    </div>
            <?php 
            if(isset($_GET['delete_id'])){
                $delete_id = $_GET['delete_id'];

                $query="SELECT * FROM `posts` WHERE post_id=$delete_id";
                $result = mysqli_query($connect,$query);
                $row=mysqli_fetch_assoc($result);
                $post_image=$row['post_image'];
                $img_path = '../image/'.$post_image;
                if(file_exists($img_path)){
                    unlink($img_path);
                }

                $query = "DELETE FROM `posts` WHERE post_id=$delete_id";
                mysqli_query($connect,$query);
                header('location:post.php');
            }
            ?>