                    <div class="col-md-12">
                        <table class="table table-responsive table-hover table-bordered">
                            <tr>
                                <th>No:</th>
                                <th>Author</th>
                                <th>Comment</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>From Response</th>
                                <th>Date</th>
                                <th>Approved</th>
                                <th>Unapproved</th>
                                <th>Delete</th>
                            </tr>
                            <?php 
                                $no = 1;
                                $query = "SELECT * FROM `comments`";
                                $result = mysqli_query($connect,$query);
                                while ($row=mysqli_fetch_assoc($result)) {
                                    $comment_id = $row['comment_id'];
                                    $comment_post_id = $row['comment_post_id'];
                                    $comment_author = $row['comment_author'];
                                    $comment_email = $row['comment_email'];
                                    $comment_content= $row['comment_content'];
                                    $comment_status = $row['comment_status'];
                                    $comment_date= $row['comment_date'];
                                
                             ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $comment_author ?></td>
                                <td><?php echo $comment_content ?></td>
                                <td><?php echo $comment_email ?></td>
                                <td><?php echo $comment_status ?></td>
                                <?php 
                                	$comment_query = "SELECT * FROM `posts` WHERE   post_id=$comment_post_id";
                                	$comment_result = mysqli_query($connect,$comment_query);
                                	while($row=mysqli_fetch_assoc($comment_result)){
                                		$post_id = $row['post_id'];
                                		$post_title = $row['post_title'];
                                	
                                 ?>
                                <td><a href="../post.php?post_id=<?php echo $post_id ?>"><?php echo $post_title ?></a></td>
                                <?php 
                                	}
                                 ?>
                                <td><?php echo $comment_date ?></td>
                                <td><a href="" class="btn btn-success">Approve</a></td>
                                <td><a href="" class="btn btn-warning">Unapprove</a></td>
                                <td><a href="" class="btn btn-danger">Delete</a></td>
                           </tr>
                            <?php 
                                }
                            ?>
                        </table>
                    </div>
            <?php 
            if(isset($_GET['delete_id'])){
                $delete_id = $_GET['delete_id'];

                $query="SELECT * FROM `posts` WHERE post_id=$delete_id";
                $result = mysqli_query($connect,$query);
                $row=mysqli_fetch_assoc($result);
                $post_image=$row['post_image'];
                $img_path = '../image/'.$post_image;
                if(file_exists($img_path)){
                    unlink($img_path);
                }

                $query = "DELETE FROM `posts` WHERE post_id=$delete_id";
                mysqli_query($connect,$query);
                header('location:post.php');
            }
            ?>